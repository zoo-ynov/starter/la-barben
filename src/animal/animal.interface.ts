import { UserData } from '../user/user.interface';
import { AnimalEntity } from './animal.entity';


interface AnimalData {
 id?: number;
 name: string;
 type: boolean;
}

export interface AnimalRO {
  animal: AnimalEntity;
}

export interface AnimalsRO {
  animal: AnimalEntity[];
}

