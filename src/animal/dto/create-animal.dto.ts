import { IsNotEmpty } from "class-validator";
import { ZooEntity } from "../../zoo/zoo.entity";
import { EnclosureEntity } from "../../enclosure/enclosure.entity";

export class CreateAnimalDto {

  @IsNotEmpty()
  readonly id: number;

  @IsNotEmpty()     
  readonly name: string;

  @IsNotEmpty()     
  readonly type: boolean;

  // @IsNotEmpty()
  readonly enclosure: EnclosureEntity; 

}
