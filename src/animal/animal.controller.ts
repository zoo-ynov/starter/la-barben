import {Get, Post, Body, Put, Delete, Query, Param, Controller, ValidationPipe, UsePipes} from '@nestjs/common';
import { Request } from 'express';
import { AnimalService } from './animal.service';
import { CreateAnimalDto } from './dto';
import { AnimalsRO, AnimalRO } from './animal.interface';

import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { AnimalEntity } from './animal.entity';

@ApiBearerAuth()
@ApiUseTags('animals')
@Controller()
export class AnimalController {

  constructor(private readonly animalService: AnimalService) {}
//GET
  @ApiOperation({ title: 'Get all animals' })
  @ApiResponse({ status: 200, description: 'Return all animals.'})
  @Get('animals')
  async findAll(): Promise<AnimalsRO> {
    return await this.animalService.findAll();
  }

  //CREATE 
  @ApiOperation({ title: 'Create animal' })
  @ApiResponse({ status: 201, description: 'The animal has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @UsePipes(new ValidationPipe())
  @Post('animals')
  async create(@Body('animal') animalData: CreateAnimalDto) {
    console.log(animalData);
    return this.animalService.create(animalData);
  }

  //UPDATE
  @ApiOperation({ title: 'Update animal' })
  @ApiResponse({ status: 201, description: 'The animal has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put('animals')
  async update( @Body('animal') animalData: CreateAnimalDto) {

    return this.animalService.update(animalData);
  }

//   DELETE
  @ApiOperation({ title: 'Delete animal' })
  @ApiResponse({ status: 201, description: 'The animal has been successfully deleted.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Delete('animals')
  async delete(@Body('animal') animalData: AnimalEntity) {
    return this.animalService.delete(animalData.id);
  }

}