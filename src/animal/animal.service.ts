import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { AnimalEntity } from './animal.entity';
import { UserEntity } from '../user/user.entity';
import { CreateAnimalDto } from './dto';

import {AnimalRO, AnimalsRO} from './animal.interface';


@Injectable()
export class AnimalService {
  constructor(
    @InjectRepository(AnimalEntity)
    private readonly AnimalRepository: Repository<AnimalEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
 
  async findAll(): Promise<AnimalsRO> {
  
    const qb = await getRepository(AnimalEntity)
      .createQueryBuilder('animal')

    const animal = await qb.getMany();
    return {animal};
  }


  // async findOne(where): Promise<AnimalRO> {
  //   const animal = await this.AnimalRepository.findOne(where);
  //   return {animal};
  // }

//   CREATE
  async create(dto: CreateAnimalDto): Promise<AnimalEntity> {

    const {id, name, type,enclosure} = dto;


    let animal = new AnimalEntity();
    animal.id = id;
    animal.name = name;
    animal.type = type;
    animal.enclosure =enclosure;


    const newAnimal = await this.AnimalRepository.save(animal);

    return newAnimal;
  }

// UPDATE
  async update(dto: CreateAnimalDto): Promise<AnimalRO> {

    const {id, name, type,enclosure} = dto;

    let updtAnimal = new AnimalEntity();
    updtAnimal.id = id;
    updtAnimal.name = name;
    updtAnimal.type = type;
    updtAnimal.enclosure =enclosure;


    let toUpdate = await this.AnimalRepository.findOne({ id: id});//{ where: { id: userId } }
    let updated = Object.assign(toUpdate, updtAnimal);
    const animal = await this.AnimalRepository.save(updated);
    return {animal};
  }

// DELETE
  async delete(id: number): Promise<DeleteResult> {
    return await this.AnimalRepository.delete({ id: id});
  }
}
