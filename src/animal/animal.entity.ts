import { Entity, PrimaryGeneratedColumn, Column, ManyToOne} from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { ZooEntity } from '../zoo/zoo.entity';
import { EnclosureEntity } from '../enclosure/enclosure.entity';

@Entity('animal')
export class AnimalEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  type: boolean;  //true: carnivore , false : herbivore 

  @ManyToOne(type => EnclosureEntity, enclosure => enclosure.animals)
  enclosure: EnclosureEntity;

}