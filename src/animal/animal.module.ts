import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AnimalController } from './animal.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalEntity } from './animal.entity';
import { UserEntity } from '../user/user.entity';
import { AnimalService } from './animal.service';
import { AuthMiddleware } from '../user/auth.middleware';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([AnimalEntity,UserEntity]), UserModule],
  providers: [AnimalService],
  controllers: [
    AnimalController
  ]
})
export class AnimalModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({path: 'animals', method: RequestMethod.DELETE});

    //  {path: 'animals', method: RequestMethod.GET},
    //  ,
    //  {path: 'animals', method: RequestMethod.PUT},
    //  {path: 'animals', method: RequestMethod.POST}
  }
}
