import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { ZooEntity } from './zoo.entity';
import { UserEntity } from '../user/user.entity';
import { CreateZooDto } from './dto';

import {ZooRO, ZoosRO} from './zoo.interface';


@Injectable()
export class ZooService {
  constructor(
    @InjectRepository(ZooEntity)
    private readonly ZooRepository: Repository<ZooEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
 
  async findAll(): Promise<ZoosRO> {
  
    const qb = await getRepository(ZooEntity)
      .createQueryBuilder('zoo')

    const zoo = await qb.getMany();
    return {zoo};
  }


  // async findOne(where): Promise<ZooRO> {
  //   const zoo = await this.ZooRepository.findOne(where);
  //   return {zoo};
  // }

//   CREATE
  async create(dto: CreateZooDto): Promise<ZooEntity> {

    const {id, name, ville,employees,enclosures} = dto;


    let zoo = new ZooEntity();
    zoo.id = id;
    zoo.name = name;
    zoo.ville = ville;
    zoo.employees =employees;
    zoo.enclosures = enclosures;

    const newZoo = await this.ZooRepository.save(zoo);

    return newZoo;
  }

// UPDATE
  async update(dto: CreateZooDto): Promise<ZooRO> {

    const {id, name, ville,employees,enclosures} = dto;


    let updtZoo = new ZooEntity();
    updtZoo.id = id;
    updtZoo.name = name;
    updtZoo.ville = ville;
    updtZoo.employees =employees;
    updtZoo.enclosures = enclosures;

    let toUpdate = await this.ZooRepository.findOne({ id: id});//{ where: { id: userId } }
    let updated = Object.assign(toUpdate, updtZoo);
    const zoo = await this.ZooRepository.save(updated);
    return {zoo};
  }

// DELETE
  async delete(id: number): Promise<DeleteResult> {
    return await this.ZooRepository.delete({ id: id});
  }
}
