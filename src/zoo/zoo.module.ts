import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ZooController } from './zoo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZooEntity } from './zoo.entity';
import { UserEntity } from '../user/user.entity';
import { ZooService } from './zoo.service';
import { AuthMiddleware } from '../user/auth.middleware';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([ZooEntity,UserEntity]), UserModule],
  providers: [ZooService],
  controllers: [
    ZooController
  ]
})
export class ZooModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({path: 'zoos', method: RequestMethod.DELETE});

      // {path: 'zoos', method: RequestMethod.GET},
      // {path: 'zoos', method: RequestMethod.DELETE},
      // {path: 'zoos', method: RequestMethod.PUT},
      // {path: 'zoos', method: RequestMethod.POST}
  }
}
