import { UserData } from '../user/user.interface';
import { ZooEntity } from './zoo.entity';


interface ZooData {
 id?: number;
 name: string;
 ville: string;
}

export interface ZooRO {
  zoo: ZooEntity;
}

export interface ZoosRO {
  zoo: ZooEntity[];
}

