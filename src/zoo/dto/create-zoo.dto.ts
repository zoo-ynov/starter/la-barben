import { IsNotEmpty } from "class-validator";
import { EnclosureEntity } from "../../enclosure/enclosure.entity";
import { EmployeeEntity } from "../../employee/employee.entity";

export class CreateZooDto {

  @IsNotEmpty()
  readonly id: number;

  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  readonly ville: string; 

  @IsNotEmpty()
  readonly employees: EmployeeEntity[];

  @IsNotEmpty()
  readonly enclosures: EnclosureEntity[];

}
