import {Get, Post, Body, Put, Delete, Query, Param, Controller, ValidationPipe, UsePipes} from '@nestjs/common';
import { Request } from 'express';
import { ZooService } from './zoo.service';
import { CreateZooDto } from './dto';
import { ZoosRO, ZooRO } from './zoo.interface';

import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { ZooEntity } from './zoo.entity';

@ApiBearerAuth()
@ApiUseTags('zoos')
@Controller()
export class ZooController {

  constructor(private readonly zooService: ZooService) {}
//GET
  @ApiOperation({ title: 'Get all zoos' })
  @ApiResponse({ status: 200, description: 'Return all zoos.'})
  @Get('zoos')
  async findAll(): Promise<ZoosRO> {
    return await this.zooService.findAll();
  }

  // @Get(':slug')
  // async findOne(@Param('slug') slug): Promise<ZooRO> {
  //   return await this.zooService.findOne({slug});
  // }

  // @Get(':slug/comments')
  // async findComments(@Param('slug') slug): Promise<CommentsRO> {
  //   return await this.zooService.findComments(slug);
  // }


  //CREATE 
  @ApiOperation({ title: 'Create zoo' })
  @ApiResponse({ status: 201, description: 'The zoo has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @UsePipes(new ValidationPipe())
  @Post('zoos')
  async create(@Body('zoo') zooData: CreateZooDto) {
    console.log(zooData);
    return this.zooService.create(zooData);
  }

  //UPDATE
  @ApiOperation({ title: 'Update zoo' })
  @ApiResponse({ status: 201, description: 'The zoo has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put('zoos')
  async update( @Body('zoo') zooData: CreateZooDto) {

    return this.zooService.update(zooData);
  }

//   DELETE
  @ApiOperation({ title: 'Delete zoo' })
  @ApiResponse({ status: 201, description: 'The zoo has been successfully deleted.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Delete('zoos')
  async delete(@Body('zoo') zooData: ZooEntity) {
    return this.zooService.delete(zooData.id);
  }

}