import { Entity, PrimaryGeneratedColumn, Column, OneToMany} from 'typeorm';
import { EmployeeEntity } from '../employee/employee.entity';
import { EnclosureEntity } from '../enclosure/enclosure.entity';

@Entity('zoo')
export class ZooEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  
  @Column()
  ville: string;

  // relation employé par zoo
  @OneToMany(type => EmployeeEntity, employee => employee.zoo)
  employees: EmployeeEntity[];

  // relation employé par zoo
  @OneToMany(type => EnclosureEntity, enclosure => enclosure.zoo)
  enclosures: EnclosureEntity[];

}