import { UserData } from '../user/user.interface';
import { EmployeeEntity } from './employee.entity';


interface EmployeeData {
 id: number;
 name: string;
 position: string;

}

export interface EmployeeRO {
  employee: EmployeeEntity;
}

export interface EmployeesRO {
  employee: EmployeeEntity[];
}

