import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { EmployeeController } from './employee.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeeEntity } from './employee.entity';
import { UserEntity } from '../user/user.entity';
import { EmployeeService } from './employee.service';
import { AuthMiddleware } from '../user/auth.middleware';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([EmployeeEntity,UserEntity]), UserModule],
  providers: [EmployeeService],
  controllers: [
    EmployeeController
  ]
})
export class EmployeeModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({path: 'employees', method: RequestMethod.DELETE});
  }
}
