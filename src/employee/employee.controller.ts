import {Get, Post, Body, Put, Delete, Query, Param, Controller, ValidationPipe, UsePipes} from '@nestjs/common';
import { Request } from 'express';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDto } from './dto';
import { EmployeesRO, EmployeeRO } from './employee.interface';

import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { EmployeeEntity } from './employee.entity';

@ApiBearerAuth()
@ApiUseTags('employees')
@Controller()
export class EmployeeController {

  constructor(private readonly employeeService: EmployeeService) {}
//GET
  @ApiOperation({ title: 'Get all employees' })
  @ApiResponse({ status: 200, description: 'Return all employees.'})
  @Get('employees')
  async findAll(): Promise<EmployeesRO> {
    return await this.employeeService.findAll();
  }

  // @Get(':slug')
  // async findOne(@Param('slug') slug): Promise<EmployeeRO> {
  //   return await this.employeeService.findOne({slug});
  // }


  //CREATE 
  @ApiOperation({ title: 'Create employee' })
  @ApiResponse({ status: 201, description: 'The employee has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @UsePipes(new ValidationPipe())
  @Post('employees')
  async create(@Body('employee') employeeData: CreateEmployeeDto) {
    console.log(employeeData);
    return this.employeeService.create(employeeData);
  }

  //UPDATE
  @ApiOperation({ title: 'Update employee' })
  @ApiResponse({ status: 201, description: 'The employee has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put('employees')
  async update( @Body('employee') employeeData: CreateEmployeeDto) {

    return this.employeeService.update(employeeData);
  }

//   DELETE
  @ApiOperation({ title: 'Delete employee' })
  @ApiResponse({ status: 201, description: 'The employee has been successfully deleted.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Delete('employees')
  async delete(@Body('employee') employeeData: EmployeeEntity) {
    return this.employeeService.delete(employeeData.id);
  }

}