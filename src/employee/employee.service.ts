import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { EmployeeEntity } from './employee.entity';
import { UserEntity } from '../user/user.entity';
import { CreateEmployeeDto } from './dto';

import {EmployeeRO, EmployeesRO} from './employee.interface';


@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(EmployeeEntity)
    private readonly EmployeeRepository: Repository<EmployeeEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
 
  async findAll(): Promise<EmployeesRO> {
  
    const qb = await getRepository(EmployeeEntity)
      .createQueryBuilder('employee')

    const employee = await qb.getMany();
    return {employee};
  }

  // async findOne(where): Promise<EmployeeRO> {
  //   const employee = await this.EmployeeRepository.findOne(where);
  //   return {employee};
  // }

//   CREATE
  async create(dto: CreateEmployeeDto): Promise<EmployeeEntity> {

    const {id, name, position,zoo} = dto;


    let employee = new EmployeeEntity();
    employee.id = id;
    employee.name = name;
    employee.position = position;
    employee.zoo =zoo;


    const newEmployee = await this.EmployeeRepository.save(employee);

    return newEmployee;
  }

// UPDATE
  async update(dto: CreateEmployeeDto): Promise<EmployeeRO> {

    const {id, name, position,zoo} = dto;

    let updtEmployee = new EmployeeEntity();
    updtEmployee.id = id;
    updtEmployee.name = name;
    updtEmployee.position = position;
    updtEmployee.zoo =zoo;

    let toUpdate = await this.EmployeeRepository.findOne({ id: id});//{ where: { id: userId } }
    let updated = Object.assign(toUpdate, updtEmployee);
    const employee = await this.EmployeeRepository.save(updated);
    return {employee};
  }

// DELETE
  async delete(id: number): Promise<DeleteResult> {
    return await this.EmployeeRepository.delete({ id: id});
  }
}
