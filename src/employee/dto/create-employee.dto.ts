import { IsNotEmpty } from "class-validator";
import { ZooEntity } from "../../zoo/zoo.entity";

export class CreateEmployeeDto {

  @IsNotEmpty()
  readonly id: number;

  @IsNotEmpty()     
  readonly name: string;
  
  @IsNotEmpty()     
  readonly position: string;

  @IsNotEmpty()
  readonly zoo: ZooEntity; 

}
