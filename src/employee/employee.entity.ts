import { Entity, PrimaryGeneratedColumn, Column, ManyToOne} from 'typeorm';

import { ZooEntity } from '../zoo/zoo.entity';

@Entity('employee')
export class EmployeeEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  position: string;

  @ManyToOne(type => ZooEntity, zoo => zoo.employees)
  zoo: ZooEntity;
  employee: ZooEntity;

}