import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult } from 'typeorm';
import { EnclosureEntity } from './enclosure.entity';
import { UserEntity } from '../user/user.entity';
import { CreateEnclosureDto } from './dto';

import {EnclosureRO, EnclosuresRO} from './enclosure.interface';

@Injectable()
export class EnclosureService {
  constructor(
    @InjectRepository(EnclosureEntity)
    private readonly EnclosureRepository: Repository<EnclosureEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
 
  async findAll(): Promise<EnclosuresRO> {
  
    const qb = await getRepository(EnclosureEntity)
      .createQueryBuilder('enclosure')

    const enclosure = await qb.getMany();
    return {enclosure};
  }


  // async findOne(where): Promise<EnclosureRO> {
  //   const enclosure = await this.EnclosureRepository.findOne(where);
  //   return {enclosure};
  // }

//   CREATE
  async create(dto: CreateEnclosureDto): Promise<EnclosureEntity> {

    const {id, name, zoo} = dto;


    let enclosure = new EnclosureEntity();
    enclosure.id = id;
    enclosure.name = name;
    enclosure.zoo = zoo;

    const newEnclosure = await this.EnclosureRepository.save(enclosure);

    return newEnclosure;
  }

// UPDATE
  async update(dto: CreateEnclosureDto): Promise<EnclosureRO> {

    const {id, name, zoo} = dto;

    let updtEnclosure = new EnclosureEntity();
    updtEnclosure.id = id;
    updtEnclosure.name = name;
    updtEnclosure.zoo = zoo;

    let toUpdate = await this.EnclosureRepository.findOne({ id: id});//{ where: { id: userId } }
    let updated = Object.assign(toUpdate, updtEnclosure);
    const enclosure = await this.EnclosureRepository.save(updated);
    return {enclosure};
  }

// DELETE
  async delete(id: number): Promise<DeleteResult> {
    return await this.EnclosureRepository.delete({ id: id});
  }
}
