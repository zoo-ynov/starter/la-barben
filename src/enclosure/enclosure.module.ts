import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { EnclosureController } from './enclosure.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnclosureEntity } from './enclosure.entity';
import { UserEntity } from '../user/user.entity';
import { EnclosureService } from './enclosure.service';
import { AuthMiddleware } from '../user/auth.middleware';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([EnclosureEntity,UserEntity]), UserModule],
  providers: [EnclosureService],
  controllers: [
    EnclosureController
  ]
})
export class EnclosureModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({path: 'enclosures', method: RequestMethod.DELETE});
  }
}
