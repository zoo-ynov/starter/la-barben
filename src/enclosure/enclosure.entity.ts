import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { ZooEntity } from '../zoo/zoo.entity';
import { AnimalEntity } from '../animal/animal.entity';

@Entity('enclosure')
export class EnclosureEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(type => ZooEntity, zoo => zoo.enclosures)
  zoo: ZooEntity;

  // relation enclos par animaux
  @OneToMany(type => AnimalEntity, animal => animal.enclosure)
  animals: AnimalEntity[];

}