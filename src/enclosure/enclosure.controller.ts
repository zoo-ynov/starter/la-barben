import {Get, Post, Body, Put, Delete, Query, Param, Controller, ValidationPipe, UsePipes} from '@nestjs/common';
import { Request } from 'express';
import { EnclosureService } from './enclosure.service';
import { CreateEnclosureDto } from './dto';
import { EnclosuresRO, EnclosureRO } from './enclosure.interface';

import {
  ApiUseTags,
  ApiBearerAuth,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { EnclosureEntity } from './enclosure.entity';

@ApiBearerAuth()
@ApiUseTags('enclosures')
@Controller()
export class EnclosureController {

constructor(private readonly enclosureService: EnclosureService) {}
//GET
  @ApiOperation({ title: 'Get all enclosures' })
  @ApiResponse({ status: 200, description: 'Return all enclosures.'})
  @Get('enclosures')
  async findAll(): Promise<EnclosuresRO> {
    return await this.enclosureService.findAll();
  }


  // @Get(':slug')
  // async findOne(@Param('slug') slug): Promise<EnclosureRO> {
  //   return await this.enclosureService.findOne({slug});
  // }



  //CREATE 
  @ApiOperation({ title: 'Create enclosure' })
  @ApiResponse({ status: 201, description: 'The enclosure has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @UsePipes(new ValidationPipe())
  @Post('enclosures')
  async create(@Body('enclosure') enclosureData: CreateEnclosureDto) {
    console.log(enclosureData);
    return this.enclosureService.create(enclosureData);
  }

  //UPDATE
  @ApiOperation({ title: 'Update enclosure' })
  @ApiResponse({ status: 201, description: 'The enclosure has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put('enclosures')
  async update( @Body('enclosure') enclosureData: CreateEnclosureDto) {

    return this.enclosureService.update(enclosureData);
  }

//   DELETE
  @ApiOperation({ title: 'Delete enclosure' })
  @ApiResponse({ status: 201, description: 'The enclosure has been successfully deleted.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Delete('enclosures')
  async delete(@Body('enclosure') enclosureData: EnclosureEntity) {
    return this.enclosureService.delete(enclosureData.id);
  }

}