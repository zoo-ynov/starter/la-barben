import { UserData } from '../user/user.interface';
import { EnclosureEntity } from './enclosure.entity';


interface EnclosureData {
 id?: number;
 name: string;
}

export interface EnclosureRO {
  enclosure: EnclosureEntity;
}

export interface EnclosuresRO {
  enclosure: EnclosureEntity[];
}

