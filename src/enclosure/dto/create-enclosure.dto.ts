import { IsNotEmpty } from "class-validator";
import { ZooEntity } from "../../zoo/zoo.entity";

export class CreateEnclosureDto {

  @IsNotEmpty()
  readonly id: number;

  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  readonly zoo: ZooEntity;
  
}
