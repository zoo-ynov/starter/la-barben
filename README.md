# Documentation projet NestJS 
#### Lannoy Vincent – Stanko Jérémy 
 
## Le projet 
Le projet a pour but de développer un outil de gestion pour un Zoo. Dans notre projet nous avons eu à développer l’outil adapté au Zoo de la Barben. Nous mettons à disposition un CRUD permettant la gestion de ces éléments : 
Elle devra gérer des zoos: 
* On peut avoir x zoos 
* Chaque zoo possède des employés (humains) 
* Chaque zoo possède des enclos 
* Chaque enclos peut avoir deux espèces d'animaux ayant le même type d'alimentation (carnivore/herbivore) 

Pour réaliser cela nous utilisera plusieurs outils :  
Pour notre API, nous utiliserons TypeOrm pour d’utiliser l’API avec TypeScript 
Nous utiliserons Postman pour essayer notre API 
Gitlab pour le versionning du projet 
 
## Déroulement du projet  
Nous avons choisi le projet “Starter” du fait de nos connaissances nouvelles en TypeScript et en termes d’API. Le projet est en 2 parties : la première avec les sources qui crée la base de données (les tables) en SQL (tout cela grâce au TypeOrm), la deuxième partie est faite avec postman, qui nous permet de nous affranchir d’un front, qui ne nous est pas utile à ce stade.  
Nous essayons donc d’utiliser TypeOrm pour la création de table, puis une fois faite, nous utilisons postman pour faire des requêtes. Le contrôleur intercepte les requêtes, appelle le service qui lui, utilisera les entêtées TypeOrm pour faire les opérations en base de données . Le fonctionnement est simple, mais nous a beaucoup appris sur le fonctionnement de NestJs. 
 
 

Groupe : STANKO Jérémy LANNOY Vincent 