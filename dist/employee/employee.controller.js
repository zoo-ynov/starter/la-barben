"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const employee_service_1 = require("./employee.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
const employee_entity_1 = require("./employee.entity");
let EmployeeController = class EmployeeController {
    constructor(employeeService) {
        this.employeeService = employeeService;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.employeeService.findAll();
        });
    }
    create(employeeData) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(employeeData);
            return this.employeeService.create(employeeData);
        });
    }
    update(employeeData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.employeeService.update(employeeData);
        });
    }
    delete(employeeData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.employeeService.delete(employeeData.id);
        });
    }
};
__decorate([
    swagger_1.ApiOperation({ title: 'Get all employees' }),
    swagger_1.ApiResponse({ status: 200, description: 'Return all employees.' }),
    common_1.Get('employees'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], EmployeeController.prototype, "findAll", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Create employee' }),
    swagger_1.ApiResponse({ status: 201, description: 'The employee has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.UsePipes(new common_1.ValidationPipe()),
    common_1.Post('employees'),
    __param(0, common_1.Body('employee')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateEmployeeDto]),
    __metadata("design:returntype", Promise)
], EmployeeController.prototype, "create", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Update employee' }),
    swagger_1.ApiResponse({ status: 201, description: 'The employee has been successfully updated.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Put('employees'),
    __param(0, common_1.Body('employee')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateEmployeeDto]),
    __metadata("design:returntype", Promise)
], EmployeeController.prototype, "update", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Delete employee' }),
    swagger_1.ApiResponse({ status: 201, description: 'The employee has been successfully deleted.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete('employees'),
    __param(0, common_1.Body('employee')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [employee_entity_1.EmployeeEntity]),
    __metadata("design:returntype", Promise)
], EmployeeController.prototype, "delete", null);
EmployeeController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('employees'),
    common_1.Controller(),
    __metadata("design:paramtypes", [employee_service_1.EmployeeService])
], EmployeeController);
exports.EmployeeController = EmployeeController;
//# sourceMappingURL=employee.controller.js.map