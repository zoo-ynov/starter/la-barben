"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const enclosure_controller_1 = require("./enclosure.controller");
const typeorm_1 = require("@nestjs/typeorm");
const enclosure_entity_1 = require("./enclosure.entity");
const user_entity_1 = require("../user/user.entity");
const enclosure_service_1 = require("./enclosure.service");
const auth_middleware_1 = require("../user/auth.middleware");
const user_module_1 = require("../user/user.module");
let EnclosureModule = class EnclosureModule {
    configure(consumer) {
        consumer
            .apply(auth_middleware_1.AuthMiddleware)
            .forRoutes({ path: 'enclosures', method: common_1.RequestMethod.GET }, { path: 'enclosures', method: common_1.RequestMethod.DELETE }, { path: 'enclosures', method: common_1.RequestMethod.PUT }, { path: 'enclosures', method: common_1.RequestMethod.POST });
    }
};
EnclosureModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([enclosure_entity_1.EnclosureEntity, user_entity_1.UserEntity]), user_module_1.UserModule],
        providers: [enclosure_service_1.EnclosureService],
        controllers: [
            enclosure_controller_1.EnclosureController
        ]
    })
], EnclosureModule);
exports.EnclosureModule = EnclosureModule;
//# sourceMappingURL=enclosure.module.js.map