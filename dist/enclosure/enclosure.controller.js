"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const enclosure_service_1 = require("./enclosure.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
const enclosure_entity_1 = require("./enclosure.entity");
let EnclosureController = class EnclosureController {
    constructor(enclosureService) {
        this.enclosureService = enclosureService;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.enclosureService.findAll();
        });
    }
    create(enclosureData) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(enclosureData);
            return this.enclosureService.create(enclosureData);
        });
    }
    update(enclosureData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enclosureService.update(enclosureData);
        });
    }
    delete(enclosureData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.enclosureService.delete(enclosureData.id);
        });
    }
};
__decorate([
    swagger_1.ApiOperation({ title: 'Get all enclosures' }),
    swagger_1.ApiResponse({ status: 200, description: 'Return all enclosures.' }),
    common_1.Get('enclosures'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], EnclosureController.prototype, "findAll", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Create enclosure' }),
    swagger_1.ApiResponse({ status: 201, description: 'The enclosure has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.UsePipes(new common_1.ValidationPipe()),
    common_1.Post('enclosures'),
    __param(0, common_1.Body('enclosure')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateEnclosureDto]),
    __metadata("design:returntype", Promise)
], EnclosureController.prototype, "create", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Update enclosure' }),
    swagger_1.ApiResponse({ status: 201, description: 'The enclosure has been successfully updated.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Put('enclosures'),
    __param(0, common_1.Body('enclosure')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateEnclosureDto]),
    __metadata("design:returntype", Promise)
], EnclosureController.prototype, "update", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Delete enclosure' }),
    swagger_1.ApiResponse({ status: 201, description: 'The enclosure has been successfully deleted.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete('enclosures'),
    __param(0, common_1.Body('enclosure')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [enclosure_entity_1.EnclosureEntity]),
    __metadata("design:returntype", Promise)
], EnclosureController.prototype, "delete", null);
EnclosureController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('enclosures'),
    common_1.Controller(),
    __metadata("design:paramtypes", [enclosure_service_1.EnclosureService])
], EnclosureController);
exports.EnclosureController = EnclosureController;
//# sourceMappingURL=enclosure.controller.js.map