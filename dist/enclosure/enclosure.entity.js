"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../user/user.entity");
const zoo_entity_1 = require("../zoo/zoo.entity");
let EnclosureEntity = class EnclosureEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EnclosureEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], EnclosureEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(type => zoo_entity_1.ZooEntity, zoo => zoo.enclosures),
    __metadata("design:type", user_entity_1.UserEntity)
], EnclosureEntity.prototype, "zoo", void 0);
EnclosureEntity = __decorate([
    typeorm_1.Entity('enclosure')
], EnclosureEntity);
exports.EnclosureEntity = EnclosureEntity;
//# sourceMappingURL=enclosure.entity.js.map