"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const zoo_entity_1 = require("./zoo.entity");
const user_entity_1 = require("../user/user.entity");
let ZooService = class ZooService {
    constructor(ZooRepository, userRepository) {
        this.ZooRepository = ZooRepository;
        this.userRepository = userRepository;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const qb = yield typeorm_2.getRepository(zoo_entity_1.ZooEntity)
                .createQueryBuilder('zoo');
            const zoo = yield qb.getMany();
            return { zoo };
        });
    }
    create(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, name, ville, enclosures, age } = dto;
            let zoo = new zoo_entity_1.ZooEntity();
            zoo.id = id;
            zoo.name = name;
            zoo.ville = ville;
            zoo.color = color;
            zoo.age = age;
            let newUser = new user_entity_1.UserEntity();
            newUser.username = username;
            newUser.email = email;
            newUser.password = password;
            newUser.articles = [];
            const newZoo = yield this.ZooRepository.save(zoo);
            return newZoo;
        });
    }
    update(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, name, race, color, age } = dto;
            let updtZoo = new zoo_entity_1.ZooEntity();
            updtZoo.id = id;
            updtZoo.name = name;
            updtZoo.race = race;
            updtZoo.color = color;
            updtZoo.age = age;
            let toUpdate = yield this.ZooRepository.findOne({ id: id });
            let updated = Object.assign(toUpdate, updtZoo);
            const zoo = yield this.ZooRepository.save(updated);
            return { zoo };
        });
    }
    delete(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.ZooRepository.delete({ id: id });
        });
    }
};
ZooService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(zoo_entity_1.ZooEntity)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], ZooService);
exports.ZooService = ZooService;
//# sourceMappingURL=zoo.service.js.map