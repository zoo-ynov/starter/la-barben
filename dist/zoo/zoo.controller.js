"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const zoo_service_1 = require("./zoo.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
const zoo_entity_1 = require("./zoo.entity");
let ZooController = class ZooController {
    constructor(zooService) {
        this.zooService = zooService;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.zooService.findAll();
        });
    }
    create(zooData) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(zooData);
            return this.zooService.create(zooData);
        });
    }
    update(zooData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.zooService.update(zooData);
        });
    }
    delete(zooData) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.zooService.delete(zooData.id);
        });
    }
};
__decorate([
    swagger_1.ApiOperation({ title: 'Get all zoos' }),
    swagger_1.ApiResponse({ status: 200, description: 'Return all zoos.' }),
    common_1.Get('zoos'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ZooController.prototype, "findAll", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Create zoo' }),
    swagger_1.ApiResponse({ status: 201, description: 'The zoo has been successfully created.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.UsePipes(new common_1.ValidationPipe()),
    common_1.Post('zoos'),
    __param(0, common_1.Body('zoo')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateZooDto]),
    __metadata("design:returntype", Promise)
], ZooController.prototype, "create", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Update zoo' }),
    swagger_1.ApiResponse({ status: 201, description: 'The zoo has been successfully updated.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Put('zoos'),
    __param(0, common_1.Body('zoo')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateZooDto]),
    __metadata("design:returntype", Promise)
], ZooController.prototype, "update", null);
__decorate([
    swagger_1.ApiOperation({ title: 'Delete zoo' }),
    swagger_1.ApiResponse({ status: 201, description: 'The zoo has been successfully deleted.' }),
    swagger_1.ApiResponse({ status: 403, description: 'Forbidden.' }),
    common_1.Delete('zoos'),
    __param(0, common_1.Body('zoo')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [zoo_entity_1.ZooEntity]),
    __metadata("design:returntype", Promise)
], ZooController.prototype, "delete", null);
ZooController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('zoos'),
    common_1.Controller(),
    __metadata("design:paramtypes", [zoo_service_1.ZooService])
], ZooController);
exports.ZooController = ZooController;
//# sourceMappingURL=zoo.controller.js.map