"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const employee_entity_1 = require("../employee/employee.entity");
const enclosure_entity_1 = require("../enclosure/enclosure.entity");
let ZooEntity = class ZooEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ZooEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ZooEntity.prototype, "name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], ZooEntity.prototype, "ville", void 0);
__decorate([
    typeorm_1.OneToMany(type => employee_entity_1.EmployeeEntity, employee => employee.zoo),
    __metadata("design:type", Array)
], ZooEntity.prototype, "employees", void 0);
__decorate([
    typeorm_1.OneToMany(type => enclosure_entity_1.EnclosureEntity, enclosure => enclosure.zoo),
    __metadata("design:type", Array)
], ZooEntity.prototype, "enclosures", void 0);
ZooEntity = __decorate([
    typeorm_1.Entity('zoo')
], ZooEntity);
exports.ZooEntity = ZooEntity;
//# sourceMappingURL=zoo.entity.js.map