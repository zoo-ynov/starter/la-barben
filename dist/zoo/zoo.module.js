"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const zoo_controller_1 = require("./zoo.controller");
const typeorm_1 = require("@nestjs/typeorm");
const zoo_entity_1 = require("./zoo.entity");
const user_entity_1 = require("../user/user.entity");
const zoo_service_1 = require("./zoo.service");
const auth_middleware_1 = require("../user/auth.middleware");
const user_module_1 = require("../user/user.module");
let ZooModule = class ZooModule {
    configure(consumer) {
        consumer
            .apply(auth_middleware_1.AuthMiddleware)
            .forRoutes({ path: 'zoos', method: common_1.RequestMethod.GET }, { path: 'zoos', method: common_1.RequestMethod.DELETE }, { path: 'zoos', method: common_1.RequestMethod.PUT }, { path: 'zoos', method: common_1.RequestMethod.POST });
    }
};
ZooModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([zoo_entity_1.ZooEntity, user_entity_1.UserEntity]), user_module_1.UserModule],
        providers: [zoo_service_1.ZooService],
        controllers: [
            zoo_controller_1.ZooController
        ]
    })
], ZooModule);
exports.ZooModule = ZooModule;
//# sourceMappingURL=zoo.module.js.map